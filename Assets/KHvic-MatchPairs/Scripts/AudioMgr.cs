using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// class to play audio
public class AudioMgr : MonoBehaviour
{
    public static AudioMgr Instance;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip[] m_audio;

    private static float vol = 1;

    void Awake()
    {
        Instance = this;
    }
    public void PlayAudio(int id)
    {
        audioSource.PlayOneShot(m_audio[id]);
    }
    public void PlayAudio(int id, float vol)
    {
        audioSource.PlayOneShot(m_audio[id], vol);
    }

}