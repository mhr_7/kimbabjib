using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gotoScene : MonoBehaviour
{
    public string sceneName;

    public bool playerIsInside;

    private void Update()
    {
        if (playerIsInside && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Lets go to: " + sceneName);
            SceneManager.LoadScene(sceneName);
        }
    }

    public void click_btn()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void click_with_params(string m_name)
    {
        SceneManager.LoadScene(m_name);
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag.ToString().ToUpper() == "PLAYER")
        {
            playerIsInside = false;
        }
    }

    public void OnTriggerStay2D(Collider2D col)
    {
        if(col.gameObject.tag.ToString().ToUpper() == "PLAYER")
        {
            playerIsInside = true;
        }
    }
}
