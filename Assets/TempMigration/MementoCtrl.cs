using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MementoCtrl : MonoBehaviour
{
    public Puzzle.Game m_Game;
    public GameObject pieces;
    public float fading_speed;
    public int scene_state, final_state;

    public Animator pieces_anim, big_anim;

    public bool cinematic_start;

    public DialogueManager m_dialogManager;
    public Dialogue m_dialogue;

    public Button goToButton;

    public string playerPrefName;

    // Start is called before the first frame update
    void Start()
    {
        cinematic_start = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Game.isFinished && !cinematic_start)
        {
            StartCoroutine(WaitForPiecesAnim());
            //StartCoroutine("WaitForPiecesAnim");
                        
            //if (!cinematic_start)
            //    StartCoroutine("StartCinematics");
            cinematic_start = true;
            scene_state++;
        }

        if(scene_state == 1)
        {
            StartCoroutine(WaitForMementoAnim());
            //StartCoroutine("WaitForMementoAnim");

            m_dialogManager.StartDialogue(m_dialogue);
            scene_state++;
        }

        //if dialog ends
        if (m_dialogManager.dialogEnded)
        {
            PlayerPrefs.SetInt(playerPrefName, 1);

            goToButton.gameObject.SetActive(true);
        }
    }

    private IEnumerator WaitForPiecesAnim()
    {
        //yield return new WaitForSeconds(3f);
        pieces_anim.SetTrigger("Hide");
        yield return new WaitForSeconds(3f);
        pieces.gameObject.SetActive(false);
    }

    private IEnumerator WaitForMementoAnim()
    {
        yield return new WaitForSeconds(3f);
        big_anim.SetTrigger("Show");
        yield return new WaitForSeconds(3f);
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
