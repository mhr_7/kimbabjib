using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportDoor : MonoBehaviour
{
    public Transform targetDoor;
    private GameObject the_player;

    private void Update()
    {
        if(the_player != null && Input.GetKeyDown(KeyCode.E))
        {
            the_player.transform.position = targetDoor.transform.position;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag.ToUpper() == "PLAYER")
        {
            the_player = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag.ToUpper() == "PLAYER")
        {
            the_player = col.gameObject;
        }
    }
}
