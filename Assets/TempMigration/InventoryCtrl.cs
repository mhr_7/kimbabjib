﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryCtrl : MonoBehaviour{
    //public item[] itemArr;
    [SerializeField]
    //public List<item> itemL;
    public item[] itemArr;

    //public int itemLimit;
    public bool viewItems, activeInv;

    public GameObject InventoryPanel;
    public GameObject[] InventoryUI;

    public int itt = 0, last_itt = 0;
    private item activeItem;

    public GameObject interract_obj;


    private void Start()
    {
        changeBoxColor();
    }
    void Update(){
        if (Input.GetKey(KeyCode.Q)){
          viewItems = true;
          //print("up arrow key is held down");

        }
        else{
          viewItems = false;
        }
        if(viewItems){ // show the inventory

            InventoryPanel.GetComponent<Animator>().SetBool("IsShowing", true);

            if(itt!= last_itt)
            {
                changeBoxColor();
            }

            if(Input.GetKeyUp(KeyCode.R)){
                itt++;
                if(itt == itemArr.Length)
                  itt = 0;
            }
            // else if(Input.GetKeyUp(KeyCode.R)){
            //
            // }
            activeItem = itemArr[itt];

            if(itemArr[itt].name != "")
              activeInv = true;
            else
              activeInv = false;
            }

        else
        {
            InventoryPanel.GetComponent<Animator>().SetBool("IsShowing", false);
        }

        if(interract_obj != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                interractOption();
            }
        }

    }

    public void interractOption()
    {
        if (interract_obj.tag == "Collectible" && !activeInv)
        {
            //getItem
            itemArr[itt] = interract_obj.GetComponent<PassiveNPC>().GetThisItem();
            activeInv = true;

            InventoryUI[itt].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = itemArr[itt].img;
            Debug.Log("get child name: " + InventoryUI[itt].transform.GetChild(1).name);

            //Destroy item
            GameObject temp = interract_obj;
            interract_obj = null;
            Destroy(temp.gameObject);
        }
        else if (interract_obj.tag == "ItemCollector" && !activeInv)
        {
            bool tempAnswer = interract_obj.GetComponent<PassiveNPC>().CheckFulfill();
            if (tempAnswer == false)
                StartCoroutine(interract_obj.GetComponent<PassiveNPC>().ShowAllTask());
        }
        else if (interract_obj.tag == "JustInstruction")
        {
            StartCoroutine(interract_obj.GetComponent<PassiveNPC>().ShowInstruction());
        }
        else if (interract_obj.tag == "ItemCollector" && activeInv)
        {
            //bool tempAnswer = col.gameObject.GetComponent<PassiveNPC>().CheckFulfill();
            bool isCorrect = false;
            //match or not
            isCorrect = interract_obj.GetComponent<PassiveNPC>().NPCRequest(itemArr[itt]);

            if (isCorrect)
            {
                useItem(activeItem);
                interract_obj.GetComponent<PassiveNPC>().CheckFulfill();
            }
            else
            {
                //Debug.Log("not match");
                StartCoroutine(interract_obj.GetComponent<PassiveNPC>().ShowAllTask());
            }

            //}
        }
    }

    public void changeBoxColor()
    {
        InventoryUI[itt].GetComponent<Image>().color = new Color32(255, 0, 0, 200);
        InventoryUI[last_itt].GetComponent<Image>().color = new Color32(127, 127, 127, 200);

        last_itt = itt;
        //Debug.Log("change color");
    }

    public void useItem(item i){
        item temp;
        temp = itemArr[itt];
        itemArr[itt] = new item(); //clear array

        InventoryUI[itt].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = null;
        //return temp;
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if(col.gameObject.tag.ToString().ToUpper() != "UNTAGGED")
        {
            Debug.Log("Exit: " + col.gameObject.name);

            interract_obj = null;
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.ToString().ToUpper() != "UNTAGGED")
        { 
            Debug.Log("Enter: " + col.gameObject.name);

            interract_obj = col.gameObject;
        }
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    switch (col.gameObject.tag)
        //    {
        //        case
        //        default:
        //            break;
        //    }
        //}
        //if(col.gameObject.tag == "ItemColelctor")
        //{

        //}
    }

    //public void OnTriggerStay2D(Collider2D col){
    ////public void OnTriggerEnter2D(Collider2D col){
    //  //if(Input.GetKey(KeyCode.E)){
    //    //Debug.Log("Inside Collision2D");
    //    //Debug.Log(col.gameObject.name);
    //    //Debug.Log(col.gameObject.tag);

    //    if (col.gameObject.tag == "Collectible" && !activeInv && Input.GetKeyDown(KeyCode.E)){
    //      // item te = new item();
    //      // te.name = col.gameObject.name;
    //      // te.img = col.gameObject.GetComponent<Image>().sprite;
    //      // addItem(te);

    //      //getItem
    //        itemArr[itt] = col.gameObject.GetComponent<PassiveNPC>().GetThisItem();
    //        activeInv = true;

    //        InventoryUI[itt].transform.GetChild(1).gameObject.GetComponent<Image>().sprite = itemArr[itt].img;
    //        Debug.Log("get child name: " + InventoryUI[itt].transform.GetChild(1).name);

    //        //Destroy item
    //        Destroy(col.gameObject);
    //    }
    //    else if(col.gameObject.tag == "ItemCollector" && !activeInv && Input.GetKeyDown(KeyCode.E))
    //    {
    //        bool tempAnswer = col.gameObject.GetComponent<PassiveNPC>().CheckFulfill();
    //        if (tempAnswer == false)
    //            StartCoroutine(col.gameObject.GetComponent<PassiveNPC>().ShowAllTask());
    //    }
    //    else if (col.gameObject.tag == "JustInstruction")
    //    {
    //        StartCoroutine(col.gameObject.GetComponent<PassiveNPC>().ShowInstruction());
    //    }
    //    else if(col.gameObject.tag == "ItemCollector" && activeInv && Input.GetKeyDown(KeyCode.E)){
    //        //bool tempAnswer = col.gameObject.GetComponent<PassiveNPC>().CheckFulfill();
    //        bool isCorrect = false;
    //        //match or not
    //        isCorrect = col.gameObject.GetComponent<PassiveNPC>().NPCRequest(itemArr[itt]);

    //        if(isCorrect){
    //            useItem(activeItem);
    //            col.gameObject.GetComponent<PassiveNPC>().CheckFulfill();
    //        }
    //        else
    //        {
    //            //Debug.Log("not match");
    //            StartCoroutine(col.gameObject.GetComponent<PassiveNPC>().ShowAllTask());
    //        }

    //        //}
    //    }
    //}
}
