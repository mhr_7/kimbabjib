using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToMemento : MonoBehaviour
{
    public string sceneName;
    public string playerPrefName;
    public GameObject mementoGameObj;

    private void Start()
    {
        if(PlayerPrefs.GetInt(playerPrefName) == 1)
        {
            mementoGameObj.SetActive(true);
        }
        else
        {
            mementoGameObj.SetActive(false);
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.transform.tag.ToUpper() == "PLAYER" && Input.GetKeyDown(KeyCode.E))
        {
            if(PlayerPrefs.GetInt(playerPrefName) == 0)
            {
                SceneManager.LoadScene(sceneName);
            }
            else
            {
                Invoke("DeleteThisGameObj", 0.5f);
            }
        }
    }

    public void DeleteThisGameObj()
    {
        if(this.transform.childCount < 1)
        {
            Destroy(this.gameObject);
        }
    }
}
