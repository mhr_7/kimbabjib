﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class item{

  public string name;
  public string img_loc;
  public Sprite img;

  public item(){
    name = "";
    img_loc = "";
  }
};
