using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MementoCardController : MonoBehaviour
{
    public CardManager m_cardManager;

    public Animator piecesAnim, big_anim;

    private bool is_cinematic;
    public int scene_state;
    public string playerPrefName;

    public DialogueManager m_dialogManager;
    public Dialogue m_dialogue;

    public Button goToButton;

    // Start is called before the first frame update
    void Start()
    {
        is_cinematic = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (m_cardManager.isWinning && !is_cinematic)
        {
            StartCoroutine(WaitForCardAnim());

            scene_state++;
            is_cinematic = true;
        }

        if(scene_state == 1)
        {
            StartCoroutine(WaitForMementoAnim());

            m_dialogManager.StartDialogue(m_dialogue);
            scene_state++;
        }

        //if dialog ends
        if (m_dialogManager.dialogEnded)
        {
            PlayerPrefs.SetInt(playerPrefName, 1);

            goToButton.gameObject.SetActive(true);
        }
    }

    private IEnumerator WaitForCardAnim()
    {
        //var anim = m_cardManager.cardList.gameObject.GetComponent<Animator>();
        //yield return new WaitForSeconds(3f);
        //anim.SetTrigger("Hide");
        piecesAnim.SetTrigger("Hide");
        yield return new WaitForSeconds(3f);
        m_cardManager.cardList.gameObject.SetActive(false);
        piecesAnim.gameObject.SetActive(false);
    }

    private IEnumerator WaitForMementoAnim()
    {
        yield return new WaitForSeconds(3f);
        big_anim.SetTrigger("Show");
        yield return new WaitForSeconds(3f);
    }
}
