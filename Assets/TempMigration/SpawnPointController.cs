using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointController : MonoBehaviour
{
    public GameObject m_spawnPoint;
    public GameObject[] spawn_loc;

    public Platformer.Mechanics.PlayerController m_playerController;

    // Start is called before the first frame update
    void Start()
    {
        int m_1 = PlayerPrefs.GetInt("memento1");
        int m_2 = PlayerPrefs.GetInt("memento2");
        int m_3 = PlayerPrefs.GetInt("memento3");
        int m_4 = PlayerPrefs.GetInt("memento4");

        if(m_4 == 1)
        {
            Debug.Log("spawned at memento 4");
            //kill
            //m_playerController.health.Die();
            m_playerController.GetComponent<Transform>().transform.position = spawn_loc[4].transform.position;
            //set the restart point
            m_spawnPoint.transform.position = spawn_loc[4].transform.position;
        }else if (m_3 == 1)
        {
            Debug.Log("spawned at memento 3");
            //m_playerController.health.Die();
            m_playerController.GetComponent<Transform>().transform.position = spawn_loc[3].transform.position;
            m_spawnPoint.transform.position = spawn_loc[3].transform.position;
        }
        else if (m_2 == 1)
        {
            Debug.Log("spawned at memento 2");
            //m_playerController.health.Die();
            m_playerController.GetComponent<Transform>().transform.position = spawn_loc[2].transform.position;
            m_spawnPoint.transform.position = spawn_loc[2].transform.position;
        }
        else if (m_1 == 1)
        {
            Debug.Log("spawned at memento 1");
            //m_playerController.health.Die();
            m_playerController.GetComponent<Transform>().transform.position = spawn_loc[1].transform.position;
            m_spawnPoint.transform.position = spawn_loc[1].transform.position;
        }
        else
        {
            Debug.Log("spawned at default");
            //m_playerController.health.Die();
            m_playerController.GetComponent<Transform>().transform.position = spawn_loc[0].transform.position;
            m_spawnPoint.transform.position = spawn_loc[0].transform.position;
        }
    }
}
